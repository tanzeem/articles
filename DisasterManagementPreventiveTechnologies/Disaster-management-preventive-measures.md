# Disaster Management, Relief and Preventive measures

> Written by Tanzeem M B, 02/11/2018

The recent incidents in Kerala lead me to research about how to take effective counter measures to predict, prevent and manage the disasters
in a effective, scientific, economic and manageable way. Though it is not scientifically possible yet to prevent a natural disaster, the loss due to the disaster can be reduced to a great extend, if it can be detected early and the effective counter measures are taken with the help of scientists and administrative officers in time. The recent incident saw a large groups of people involving in the voluntary rescue operations which means the people here are counter-productive and efficient to deal with similar situations. All were one in the rescue and disaster management operations and those who tried to take political allegations found themselves "fishing in troubled waters" and became a funny laughing stock in front of the public. Recently in Aug mid 2018, our state of Kerala in India saw the largest flood of the century, taking the lives of many, and  leaving thousands homeless and their commodiies washed away. Another incident was the fire which broke out in Monvila on 31 Oct 2018 at an Industrial centre which couldnt be met with rightly. The flood which caused because of heavy rain, opening of dam water, low pressure, which lead to land slides and floods mostly affected by the districts in Kerala mainly Palakkad, Idukki, Malappuram, Alappuzha, Ernakulam, Kottayam, Wayanad and Kozhikkode with Red Alert; the districts with dams were the most affected. Since Idukki and Palakkad had 12 and 13 dams. The Mullapperiar dam opening by Tamil Nadu was also concerned as a major reason for the floods. The second incident of Fire in the gave insight in the matter that most of the industrial organisation doesnt take the necessary preventive measures as directed by the Kerala Fire and Rescue Dept. The fire that broke out in the Industrial organisation at Monvila, Thiruvananthapuram which is near to many organisations and institutions like the Technopark, Keltron, College of Engineering Thiruvanthapuram and many schools around the area. Though the right acting of surrounding people especially from those of Technopark who informed the Fire Force about the incident in time, which prevented further spreading of Fire while the industry was fully burnt away. The KFRSD has recently done a study and give a notice to the organisation which is only what they have the legal power to do, but the organisation didnt make proper counter measures in time which led to the fire accident there. The solution is to enforce counter measures and security standards in all organisations based on their size and type as specified in the web site of KFRSD and seal an office from use if the security measures arent met within a tipualted period. The present system installed in many organistions work in a way in which people will inform the Fire Force through the 101 number and the local Fire Force will come with their force to do the fire extinguishing works. In the incident at Monvila, more help was taken through the Fire Dept of VSSC which is very near to Monvila and the Fire Force from TamilNadu. In many incidents reported, by the time the force would have arrived, most of the damage would have happened.
So a proper measure would be automatic fire extinguishing solutions. The following sections describes some of the solutions which may be used in a variety of disaster incident situations. The first four(ATST, OpenRelief UAV, C-Water, and Disaster Relief Toilet) among the following could be used for disasters like floods, earthquakes and storms. The remaining are focussed in dealing with fire accidents through automatic fire extinguishing devices. Japanese research and technologies may be referred frequently for their effective solutions for disaster relief for Reseach and Development of solutions in the area.

## Common Disaster Relief Solutions for Floods, Storms and Earthquakes

[Reference](https://www.popularmechanics.com/science/environment/g1025/9-disaster-relief-inventions/?slide=1)

dated @2012

tl;dr

* ATST
* OpenRelief
* C-Water
* Disaster Relief Toilets


### The All Terrain Solar Trailer (ATST)

![atst](01-ATST.jpg)

* Developed by Michael and Kenny Ham (the guys behind ApocalypsEV, a compact all-terrain vehicle for the end of the world) 

* __Purpose__: Helps victims of natural disasters `stay connected`, even `during lengthy power outages`. 

* __What is ATST__: A portable, solar-powered-generator station that can charge up to 100 phones at once `using both batteries and the sun`. 
- Designed to be towed behind the brothers' ApocalypsEV ATVs, which also run on a combo of battery and solar energy.
- The ATST's would be able to reach places where infrastructure has been destroyed. 

* __Specifications__:

- Each trailer will come equipped with either `four or eight batteries`,
- providing `1200 to 2400 watts` of `solar charge`,
- and feature `100 outlets`:
  - `50` of them `regular 110-volt` outlets
  - and the `others` a `combination` of `USB`, `iPhone`, and `lesser-used connections`.
* Future Plans
 > "To give these trailers a grid-tie ability that will allow them to pay for themselves over time by lowering the electric bill of any organization that uses them," Kenny Ham says.

### OpenRelief UAV

[See Link](http://www.opendawn.com/openrelief/)

![open-reliefUAV](02-UAV.jpg)

OpenRelief is `crowd-sourcing a drone`. The organization is creating a UAV that can help with disaster relief. The miniature plane, `built` mostly from off-the-shelf components such as `fiberglass`, has a `5.5-foot wingspan` and `weighs less than 7 pounds`. It will `use advanced image processing systems` to `identify` things like `roads`, `smoke`, and `people`, and `record what it sees`. Equipped with an `open-source system` called `ArduPilot Mega`, the `UAV` can be easily `controlled using GPS and IMU sensors—electronic devices` that can `also measure` an area's `radiation levels and weather`.

### C-Water
![c-water](03-c-water.jpg)

Pure drinking water is a precious resource after a disaster. And while there are numerous filters on the market, the ease and simplicity of Chinese engineer Chao Gao's C-Water prototype, which took second place in Designboom's 2010 Incheon International Design Awards, makes it great for an emergency. Placed on the ground or in water, Gao's lightweight collapsible device accumulates water vapor inside its filter. The sun then heats the vapor—which eventually condenses on the roof of C-Water—and purifies it. Two days later—after exposure to the sun has killed nearly all the microbes—the H20 is safe to consume.

### LuminAID
![lumin-aid](05-lumin-aid.jpg)

The LuminAID is a solar-powered inflatable LED that's waterproof and floats, which makes air distribution easy. This inventive and resourceful way to illuminate a disaster zone is created by Columbia University architecture students Anna Stork and Andrea Sreshta. There are also no movable parts, and each LuminAID can provide light for up to three years without replacement. An inflatable case when used with it, softens the harsh LED light. Visit www.luminaid.com for more information.

### Disaster Relief Toilet
![disaster-relief-toilet](04-disaster-relief-toilets.jpg)

Industrial designer Rahim Bhimani created the Disaster Relief Toilet after witnessing the horrors of people having nowhere to dispose of their personal waste during the 2010 Chilean earthquake. His easy-to-assemble "comfort station" consists of an 18-inch-high flat-pack toilet, an encompassing tent for privacy, and a waste-disposal system that includes an easy-to-remove cart and a biodegradable drawsting bag. A person can piece together the plastic toilet with a coin or a butter knife, and it's ready for use.

### DARPA CAAT
![darpa-caat](06-DARPA-CAAT.jpg)

DARPA's Captive Air Amphibious Transporter (CAAT) could make delivering disaster aid from commercial container ships vastly more efficient. How? Amphibious tanks. The CAATs are humongous vehicles (each one is expected to weigh 450 tons and measure 50 yards long) equipped with air-filled treads that provide buoyancy, allowing the CAAT to roll atop water and onto land. And with so much space, they'll be able to carry massive amounts of cargo. They will also sport snorkels—especially handy during severe storms.

## Project OWL

Project OWL, an IoT and software solution that keeps first responders and victims connected in a natural disaster, has won the 2018 Call for Code Global Challenge. The team takes home the USD$200,000 grand prize and the opportunity to deploy the solution through the IBM Corporate Service Corps, among other benefits.

“We don’t view this as the finish line for our work but a checkpoint in a journey,” said Bryan Knouse, Project OWL team lead. “I can’t wait to get home so we can get back to work on the technology and the solutions for those who need it most.”

Knouse knew he wanted to develop solutions to help people cope after natural disasters, but he had trouble finding venture capitalists willing to invest in these projects. When he learned about Call for Code, he jumped at the opportunity to get involved and join the affiliated Slack community.

There, he met a group of like-minded individuals – Magus Pereira, Nicholas Feuer, Charlie Evans, and Taraqur Rahman – and Project OWL was born.

The team addresses a fundamental question that arises in the wake of a natural disaster: How do you maintain critical operations and communications when the power is cut and cell connectivity fails?

“We really were inspired by the whole hurricane situation in Puerto Rico,” Feuer said. “All communication was down. It was completely dark for not just one week, but weeks and into months.”

How it works

Project OWL, which stands for Organization, Whereabouts, and Logistics, is a two-part hardware/software solution. It provides an offline communication infrastructure that gives first responders a simple interface for managing all aspects of a disaster.

The physical “clusterduck” network is made of hubs resembling rubber ducks, which can float in flooded areas if needed. Only five are needed to cover a square mile, and they create a mesh network that can send speech-based communications using conversational systems (like Alexa and Facebook Messenger) to a central application. This application, the OWL software incident management system, uses predictive analytics and multiple data sources to build a dashboard for first responders.

“Once this network of ducks is deployed and then clustered, civilians are able to basically get on the devices through a really intuitive interface and contact first responders with a list of things that are really essential to them,” Pereira said.

With this information, Project OWL allows first responders to manage a disaster, coordinate resources, learn about weather patterns and get information data analytics through the cloud. The solution bakes in the latest IBM Watson Studio, Watson Cloud APIs, and Weather Company APIs – all built on the IBM Cloud.

Weather data forms a core part of the application, with the ability to ask questions like “Which direction is the nearest tropical storm is headed?” and “What conditions can you expect tomorrow night?” after hurricane flooding.

“In the worst disasters, chaos and misinformation are pervasive,” Knouse said. “With better information and better analytics, you can get the resources you need to the places that need it most. This type of efficiency can dramatically impact the number of people that can be saved in a disaster.”
What’s next for the team

The team is working on testing OWL in simulated environments with response teams. They plan to roll out to small incidents before deploying in full-scale disasters. Their goal is to focus on regions where annual weather patterns consistently impact communities negatively, such as India, China, the Philippines, and parts of the U.S.

In addition to the cash prize and support from the IBM Corporate Service Corps, the team members, who hail from New York and North Carolina, will have the opportunity to pitch OWL to venture capitalist firm NEA for potential funding.

“Throughout its history, IBM has believed in the ingenuity of curious people to improve humanity with forward-thinking technology. Moreover, from driving collaboration on Linux and Java to Kubernetes and Hyperledger, IBM has strongly believed in the importance of working openly so that everyone can benefit from the best ideas,” said IBM Chief Digital Officer Bob Lord. “Today, with the ability to safely process data at scale using sophisticated tools like AI, cloud, blockchain, and IoT, developers are unleashing the power of IBM’s open code to effect change faster, in more places, and in more meaningful ways than ever before.”

Daryl Pereira, Kevin Allen and Liz Klipp contributed reporting to this article.

Components IBM Cloud Node-RED

How it works

Project OWL, which stands for Organization, Whereabouts, and Logistics, is a two-part hardware/software solution. It provides an offline communication infrastructure that gives first responders a simple interface for managing all aspects of a disaster.

The physical “clusterduck” network is made of hubs resembling rubber ducks, which can float in flooded areas if needed. Only five are needed to cover a square mile, and they create a mesh network that can send speech-based communications using conversational systems (like Alexa and Facebook Messenger) to a central application. This application, the OWL software incident management system, uses predictive analytics and multiple data sources to build a dashboard for first responders.

“Once this network of ducks is deployed and then clustered, civilians are able to basically get on the devices through a really intuitive interface and contact first responders with a list of things that are really essential to them,” Pereira said.

With this information, Project OWL allows first responders to manage a disaster, coordinate resources, learn about weather patterns and get information data analytics through the cloud. The solution bakes in the latest IBM Watson Studio, Watson Cloud APIs, and Weather Company APIs – all built on the IBM Cloud.

Weather data forms a core part of the application, with the ability to ask questions like “Which direction is the nearest tropical storm is headed?” and “What conditions can you expect tomorrow night?” after hurricane flooding.

“In the worst disasters, chaos and misinformation are pervasive,” Knouse said. “With better information and better analytics, you can get the resources you need to the places that need it most. This type of efficiency can dramatically impact the number of people that can be saved in a disaster.”
What’s next for the team

The team is working on testing OWL in simulated environments with response teams. They plan to roll out to small incidents before deploying in full-scale disasters. Their goal is to focus on regions where annual weather patterns consistently impact communities negatively, such as India, China, the Philippines, and parts of the U.S.

In addition to the cash prize and support from the IBM Corporate Service Corps, the team members, who hail from New York and North Carolina, will have the opportunity to pitch OWL to venture capitalist firm NEA for potential funding.

“Throughout its history, IBM has believed in the ingenuity of curious people to improve humanity with forward-thinking technology. Moreover, from driving collaboration on Linux and Java to Kubernetes and Hyperledger, IBM has strongly believed in the importance of working openly so that everyone can benefit from the best ideas,” said IBM Chief Digital Officer Bob Lord. “Today, with the ability to safely process data at scale using sophisticated tools like AI, cloud, blockchain, and IoT, developers are unleashing the power of IBM’s open code to effect change faster, in more places, and in more meaningful ways than ever before.”


# Fire Disaster Solutions

Many organisations in Kerala don't even meet the present standards. Also, the present solutions available in many installations are a fire alarm system and has to be manually operated.The Fire Service will put it out when called but the smoke damage occurring before they arrive and the water damage after can be quite significant. Better systems can be deployed in this era of smart system solutions. The topic of discussion here is about what solutions can be developed and used in situations when fire alarms are just not enough and which is always not enough. This is where Fire Suppression systems comes into the scene. Active Fire Protections are used in many areas and machines though not limited to Server Rooms, Data Centres, Communication Rooms, UPS Rooms, Industrial Processes, Manufacturing, Electrical Switch Cabinets, Restaurent Kitchens using electrical systems. fire extinguishing systems & automatic extinguishing systems are used to reduce the level of damage and down time by automatically suppressing the fire ready for the Fire Service to get there and ensure it is out and safe to return.

Active fire protection (AFP) is an integral part of fire protection. AFP is characterized by items and/or systems, which require a certain amount of motion and response in order to work, contrary to passive fire protection.

### References

https://www.popularmechanics.com/science/environment/g1025/9-disaster-relief-inventions/?slide=1

https://developer.ibm.com/blogs/2018/10/26/with-project-owl-a-smart-network-of-rubber-ducks-can-save-lives/

https://en.wikipedia.org/wiki/Fire_sprinkler_system

https://en.wikipedia.org/wiki/Active_fire_protection

https://en.wikipedia.org/wiki/Passive_fire_protection

https://www.firetrace.com/

http://www.conceptfire-uk.com/fire-extinguishing-systems/

https://www.firepro.com/
